<?php

namespace App\Services;


use Amp\Artax\DefaultClient;
use function Amp\call;
use Amp\Promise;

class CurrencyService
{
    private const END_POINT = 'https://free.currencyconverterapi.com/api/v6/convert';

    public function getConvertedValue(array $params): Promise
    {
        return call(function () use ($params) {
            $currencyPair = $params['currency_from'] . '_' . $params['currency_to'];

            $url = static::END_POINT
                . '?q=' . $currencyPair
                . '&compact=ultra'
                . '&date=' . $params['date'];

            $response = yield $this->makeRequest($url);

            $convertedValue = 0;
            if (isset($response[$currencyPair])
                && isset($response[$currencyPair][$params['date']])) {
                $convertedValue = $params['value'] * $response[$currencyPair][$params['date']];
            }

            return $convertedValue;
        });
    }

    private function makeRequest(string $url): Promise
    {
        $client = new DefaultClient;
        return call(function () use ($client, $url) {
            $response = yield $client->request($url);
            $body = yield $response->getBody();
            if (empty($body)) {
                $body = [];
            } else {
                $body = json_decode($body, true);
            }
            return $body;
        });
    }
}