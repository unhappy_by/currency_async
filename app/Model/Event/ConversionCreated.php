<?php

namespace App\Model\Event;

use Prooph\EventSourcing\AggregateChanged;

class ConversionCreated extends AggregateChanged
{
    public function id(): int
    {
        return $this->payload['id'];
    }

    public function currency_from(): string
    {
        return $this->payload['currency_from'];
    }

    public function currency_to(): string
    {
        return $this->payload['currency_to'];
    }

    public function value(): float
    {
        return $this->payload['value'];
    }

    public function converted_value(): float
    {
        return $this->payload['converted_value'];
    }

    public function date(): string
    {
        return $this->payload['date'];
    }
}