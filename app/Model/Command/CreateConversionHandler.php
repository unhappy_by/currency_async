<?php

namespace App\Model\Command;

use App\Model\Conversion;
use App\Model\ConversionRepository;

class CreateConversionHandler
{
    private $repository;

    public function __construct(ConversionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(CreateConversion $createConversion): void
    {
        $conversion = Conversion::createWithData(
            $createConversion->id(),
            $createConversion->currency_from(),
            $createConversion->currency_to(),
            $createConversion->value(),
            $createConversion->converted_value(),
            $createConversion->date()
        );

        $this->repository->save($conversion);
    }
}