<?php

namespace App\Model;

use App\Model\Event\ConversionCreated;
use Prooph\EventSourcing\AggregateChanged;
use Prooph\EventSourcing\AggregateRoot;

class Conversion extends AggregateRoot
{
    private $id, $currency_from, $currency_to, $value, $converted_value, $date;


    public static function createWithData(int $id, string $currency_from, string $currency_to, float $value, float $converted_value, string $date): self
    {
        $obj = new self;
        $obj->recordThat(ConversionCreated::occur((string)$id, [
            'currency_from' => $currency_from,
            'currency_to' => $currency_to,
            'value' => $value,
            'converted_value' => $converted_value,
            'date' => $date,
        ]));

        return $obj;
    }

    protected function aggregateId(): string
    {
        return $this->id;
    }

    protected function apply(AggregateChanged $event): void
    {
        switch (get_class($event)) {
            case ConversionCreated::class:
                /** @var ConversionCreated $event */
                $this->id = $event->aggregateId();
                $this->currency_from = $event->currency_from();
                $this->currency_to = $event->currency_to();
                $this->value = $event->value();
                $this->converted_value = $event->converted_value();
                $this->date = $event->date();
                break;
        }
    }
}