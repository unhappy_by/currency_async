<?php

namespace App\Model;

interface ConversionRepository
{
    public function save(Conversion $conversion): void;
    public function get(string $id): ?Conversion;
}