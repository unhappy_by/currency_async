<?php

namespace App\Infrastructure;

use App\Model\Conversion;
use PDO;
use Prooph\EventSourcing\Aggregate\AggregateRepository;
use Prooph\EventSourcing\Aggregate\AggregateType;
use Prooph\EventSourcing\EventStoreIntegration\AggregateTranslator;
use Prooph\EventStore\EventStore;
use Prooph\SnapshotStore\SnapshotStore;
use App\Model\ConversionRepository as BaseConversionRepository;

class ConversionRepository extends AggregateRepository implements BaseConversionRepository
{
    private $pdo;

    public function __construct(EventStore $eventStore, SnapshotStore $snapshotStore, PDO $pdo)
    {
        parent::__construct(
            $eventStore,
            AggregateType::fromAggregateRootClass(Conversion::class),
            new AggregateTranslator(),
            $snapshotStore,
            null,
            true
        );

        $this->pdo = $pdo;
    }

    public function save(Conversion $conversion): void
    {
        $this->saveAggregateRoot($conversion);
    }

    public function get(string $id): ?Conversion
    {
        return $this->getAggregateRoot($id);
    }

    public function getConversionByParams(array $params): array
    {
        $query = $this->pdo->prepare('SELECT * FROM `read_conversions` WHERE `currency_from` = ? AND `currency_to` = ? AND `value` = ? AND `date` = ?');
        $query->bindValue(1, $params['currency_from']);
        $query->bindValue(2, $params['currency_to']);
        $query->bindValue(3, $params['value']);
        $query->bindValue(4, $params['date']);
        $query->execute();
        $result = $query->fetch();
        if (!$result) {
            return [];
        }
        return $result;
    }
}