<?php

namespace App\Projection;

use App\Model\Event\ConversionCreated;

class ConversionProjector
{
    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function onConversionCreated(ConversionCreated $conversionCreated): void
    {
        $query = $this->pdo->prepare('INSERT INTO `read_conversions` SET `id` = ?, `currency_from` = ?, `currency_to` = ?, `value` = ?, `converted_value` = ?, `date` = ?');
        $query->bindValue(1, $conversionCreated->aggregateId());
        $query->bindValue(2, $conversionCreated->currency_from());
        $query->bindValue(3, $conversionCreated->currency_to());
        $query->bindValue(4, $conversionCreated->value());
        $query->bindValue(5, $conversionCreated->converted_value());
        $query->bindValue(6, $conversionCreated->date());
        $query->execute();
    }
}