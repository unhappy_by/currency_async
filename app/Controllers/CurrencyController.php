<?php

namespace App\Controllers;


use function Amp\call;
use Amp\Http\Server\Request;
use Amp\Http\Server\Response;
use Amp\Http\Status;
use Amp\Promise;
use App\Infrastructure\ConversionRepository;
use App\Model\Command\CreateConversion;
use App\Model\Command\CreateConversionHandler;
use App\Model\Conversion;
use App\Model\Event\ConversionCreated;
use App\Projection\ConversionProjector;
use App\Services\CurrencyService;
use PDO;
use Prooph\Common\Event\ProophActionEventEmitter;
use Prooph\Common\Messaging\FQCNMessageFactory;
use Prooph\EventStore\ActionEventEmitterEventStore;
use Prooph\EventStore\Pdo\MySqlEventStore;
use Prooph\EventStore\Pdo\PersistenceStrategy\MySqlAggregateStreamStrategy;
use Prooph\EventStoreBusBridge\EventPublisher;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\EventBus;
use Prooph\ServiceBus\Plugin\Router\CommandRouter;
use Prooph\ServiceBus\Plugin\Router\EventRouter;
use Prooph\SnapshotStore\Pdo\PdoSnapshotStore;

class CurrencyController
{
    private $service;

    private $conversionRepository;

    private $commandBus;

    public function __construct()
    {
        $this->service = new CurrencyService();

        $pdo = new PDO('mysql:dbname='.DBNAME.';host='.HOST, USERNAME, PASSWORD);

        $eventStore = new MySqlEventStore(new FQCNMessageFactory(), $pdo, new MySqlAggregateStreamStrategy());
        $eventEmitter = new ProophActionEventEmitter();
        $eventStore = new ActionEventEmitterEventStore($eventStore, $eventEmitter);

        $eventBus = new EventBus($eventEmitter);
        $eventPublisher = new EventPublisher($eventBus);
        $eventPublisher->attachToEventStore($eventStore);

        $pdoSnapshotStore = new PdoSnapshotStore($pdo);
        $this->conversionRepository = new ConversionRepository($eventStore, $pdoSnapshotStore, $pdo);

        $commandBus = new CommandBus();
        $router = new CommandRouter();
        $router->route(CreateConversion::class)->to(new CreateConversionHandler($this->conversionRepository));
        $router->attachToMessageBus($commandBus);
        $this->commandBus = $commandBus;

        $conversionProjector = new ConversionProjector($pdo);
        $eventRouter = new EventRouter();
        $eventRouter->route(ConversionCreated::class)->to([$conversionProjector, 'onConversionCreated']);
        $eventRouter->attachToMessageBus($eventBus);
    }

    public function handleRequest(Request $request): Promise
    {
        return call(function () use ($request) {
            $params = $this->getQueryParams($request);

            $errors = $this->validate($params);
            if (!empty($errors)) {
                $response = $this->formResponse(Status::UNPROCESSABLE_ENTITY, [
                    'status' => Status::UNPROCESSABLE_ENTITY,
                    'errors' => $errors,
                ]);
                return new Response($response['status'], ['content-type' => 'application/json'], $response['body']);
            }

            $params['date'] = isset($params['date']) ? $params['date'] : date('Y-m-d');

            $convertion = $this->getConvertion($params);
            if (!isset($convertion['converted_value'])) {
                $convertedValue = yield $this->service->getConvertedValue($params);
                $params['converted_value'] = $convertedValue;
            } else {
                $params['converted_value'] = $convertion['converted_value'];
            }

            $this->createConvertion($params);

            $response = $this->formSuccessResponse($params);
            return new Response($response['status'], ['content-type' => 'application/json'], $response['body']);
        });
    }

    private function getConvertion(array $params): array
    {
        return $this->conversionRepository->getConversionByParams($params);
    }

    private function createConvertion(array $params): void
    {
        $this->commandBus->dispatch(new CreateConversion([
            'id' => rand(1, 1000000),
            'currency_from' => $params['currency_from'],
            'currency_to' => $params['currency_to'],
            'value' => (float)$params['value'],
            'converted_value' => $params['converted_value'],
            'date' => isset($params['date']) ? $params['date'] : date('Y-m-d'),
        ]));
    }

    private function formSuccessResponse(array $params): array
    {
        $body = [
            'status' => Status::OK,
            'result' => [
                'currency_from' => $params['currency_from'],
                'currency_to' => $params['currency_to'],
                'value' => $params['value'],
                'converted_value' => $params['converted_value'],
                'date' => isset($params['date']) ? $params['date'] : date('Y-m-d'),
            ],
        ];

        return $this->formResponse(Status::OK, $body);
    }

    private function formResponse(int $status, array $body): array
    {
        return [
            'status' => $status,
            'body' => json_encode($body),
        ];
    }

    private function getQueryParams(Request $request): array
    {
        $queryString = $request->getUri()->getQuery();
        $queryArray = explode('&', $queryString);

        $params = [];
        foreach ($queryArray as $query) {
            $param = explode('=', $query);
            if (count($param) !== 2) {
                continue;
            }
            $value = end($param);
            $key = reset($param);
            $params[$key] = $value;
        }
        return $params;
    }

    private function validate(array $params): array
    {
        $errors = [];
        foreach ($this->rules() as $attribute => $rules) {
            foreach (explode('|', $rules) as $rule) {
                if ($rule == 'required' && empty($params[$attribute])) {
                    $errors[] = [
                        'status' => 422,
                        'title' => 'Required Attribute',
                        'detail' => $attribute.' is required',
                    ];
                    continue 2;
                }
                if ($rule == 'string' && !is_string($params[$attribute])) {
                    $errors[] = [
                        'status' => 422,
                        'title' => 'Invalid Attribute',
                        'detail' => $attribute.' must be a number',
                    ];
                }
                if ($rule == 'numeric' && !is_numeric($params[$attribute])) {
                    $errors[] = [
                        'status' => 422,
                        'title' => 'Invalid Attribute',
                        'detail' => $attribute.' must be a number',
                    ];
                }
            }
        }
        return $errors;
    }

    private function rules(): array
    {
        return [
            'currency_from' => 'required|string',
            'currency_to' => 'required|string',
            'value' => 'required|numeric',
        ];
    }
}