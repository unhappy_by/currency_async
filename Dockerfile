FROM php:7.2-cli

RUN docker-php-ext-install pcntl pdo pdo_mysql
RUN docker-php-ext-enable pcntl pdo pdo_mysql

#ADD ./ /var/www/app

#ENV MYSQL_DATABASE currency_async
#ENV MYSQL_ROOT_PASSWORD root

#COPY ./sql-scripts/ /docker-entrypoint-initdb.d/

CMD cd /var/www/app && php server.php

EXPOSE 1337